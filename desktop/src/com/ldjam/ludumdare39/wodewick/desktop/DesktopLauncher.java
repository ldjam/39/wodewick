package com.ldjam.ludumdare39.wodewick.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.ldjam.ludumdare39.wodewick.Game;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.height = Game.HEIGHT;
		config.width = Game.WIDTH;
		config.title = Game.TITLE;
		new LwjglApplication(new Game(), config);
	}
}
