package com.ldjam.ludumdare39.wodewick;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.ScalingViewport;
import com.ldjam.ludumdare39.wodewick.actors.*;

import java.util.ArrayList;
import java.util.List;

public class Game extends ApplicationAdapter implements ContactListener {
    public static final int HEIGHT = 600;
    public static final int WIDTH = 600;
    public static final String TITLE = "Wodewick, the grumpy monster";
    public static final float SCALING_FACTOR = 10;
    private static final long HUGGER_SPAWN_TIME_MILLISECONDS = 1000;
    private static final long HUGGING_DURATION_MILLISECONDS = 1000;

    private World world;
    private Stage gameStage;
    private List<Hugger> huggers = new ArrayList<>();
    private Wodewick wodewick;
    private PowerBar powerBar;
    private LivesBar livesBar;
    private long nextHuggerTime;
    private Music ingameMusic;
    private Stage uiStage;
    private Music startScreenMusic;
    private GameState state;
    private Image startScreen;
    private long startTime;
    private long huggingStart;

    enum GameState {
        START_SCREEN,
        IN_GAME,
        HUGGING, GAME_OVER
    }

    @Override
    public void create() {
        state = GameState.START_SCREEN;
        uiStage = new Stage();

        Gdx.input.setInputProcessor(new InputAdapter() {
            @Override
            public boolean keyDown(int keycode) {
                return handleKeyDown(keycode);
            }
        });

        ingameMusic = Gdx.audio.newMusic(Gdx.files.internal("music/evil-rock.mp3"));
        ingameMusic.setLooping(true);

        startScreenMusic = Gdx.audio.newMusic(Gdx.files.internal("music/sad-grumpy.mp3"));
        startScreenMusic.setLooping(true);
        startScreenMusic.play();

        startScreen = new Image(new Texture("start_screen.png"));
        uiStage.addActor(startScreen);
    }

    private void start() {
        uiStage.clear();

        startScreenMusic.stop();
        ingameMusic.play();

        world = new World(new Vector2(0, 0), true);
        world.setContactListener(this);

        ScalingViewport viewport = new ScalingViewport(
                Scaling.stretch,
                Gdx.graphics.getWidth() / SCALING_FACTOR,
                Gdx.graphics.getHeight() / SCALING_FACTOR
        );
        gameStage = new Stage(viewport);

        wodewick = new Wodewick(world);
        gameStage.addActor(wodewick);

        livesBar = new LivesBar(wodewick);
        gameStage.addActor(livesBar);

        powerBar = new PowerBar(wodewick);
        gameStage.addActor(powerBar);

        nextHuggerTime = System.currentTimeMillis();

        state = GameState.IN_GAME;
        startTime = System.currentTimeMillis();
    }

    private boolean handleKeyDown(int keycode) {
        if (state == GameState.IN_GAME) {
            switch (keycode) {
                case Input.Keys.DOWN:
                    wodewick.push(Direction.DOWN);
                    return true;
                case Input.Keys.LEFT:
                    wodewick.push(Direction.LEFT);
                    return true;
                case Input.Keys.RIGHT:
                    wodewick.push(Direction.RIGHT);
                    return true;
                case Input.Keys.UP:
                    wodewick.push(Direction.UP);
                    return true;
            }
        } else {
            switch (keycode) {
                case Input.Keys.ENTER:
                    start();
                    return true;
            }
        }

        return false;
    }

    @Override
    public void render() {
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        if (state == GameState.IN_GAME) {
            if (nextHuggerTime < System.currentTimeMillis()) {
                Hugger hugger = new Hugger(world);
                huggers.add(hugger);
                gameStage.addActor(hugger);

                nextHuggerTime = System.currentTimeMillis() + HUGGER_SPAWN_TIME_MILLISECONDS;
            }

            world.step(1 / 60f, 6, 2);

            gameStage.act();
            gameStage.draw();
        } else {
            uiStage.act();
            uiStage.draw();
        }

        if ((state == GameState.HUGGING) && (System.currentTimeMillis() - huggingStart > HUGGING_DURATION_MILLISECONDS)) {
            ingameMusic.play();
            state = GameState.IN_GAME;
        }
    }

    @Override
    public void dispose() {
        if (state == GameState.IN_GAME) {
            disposeGameStage();
        }

        uiStage.dispose();
        ingameMusic.dispose();
        startScreenMusic.dispose();
    }

    private void disposeGameStage() {
        gameStage.dispose();
        huggers.forEach(Hugger::dispose);
        huggers.clear();
        wodewick.dispose();
    }

    @Override
    public void beginContact(Contact contact) {
        Object objectA = contact.getFixtureA().getUserData();
        Object objectB = contact.getFixtureB().getUserData();

        if ((objectA != wodewick) && (objectB != wodewick)) {
            return;
        }

        if (wodewick.isAttacking) {
            return;
        }

        Hugger hugger;
        if (objectA instanceof Hugger) {
            hugger = (Hugger) objectA;
        } else if (objectB instanceof Hugger) {
            hugger = (Hugger) objectB;
        } else {
            return;
        }

        gameStage.getActors().removeValue(hugger, true);
        hugger.dispose();

        wodewick.hug();

        if (wodewick.numLives < 1) {
            gameOver();
        } else {
            state = GameState.HUGGING;
            huggingStart = System.currentTimeMillis();

            ingameMusic.pause();

            uiStage.clear();

            Image image = new Image(new Texture("hug_scene.png"));
            uiStage.addActor(image);
        }
    }

    private void gameOver() {
        long survivedSeconds = (System.currentTimeMillis() - startTime) / 1000;

        ingameMusic.stop();
        startScreenMusic.play();

        uiStage.clear();

        Image image = new Image(new Texture("game_over_scene.png"));
        uiStage.addActor(image);

        Label.LabelStyle labelStyle = new Label.LabelStyle();
        labelStyle.fontColor = new Color(0, 0, 0, 0.9f);
        labelStyle.font = new BitmapFont();
        Label label = new Label("You survived the hugging for " + survivedSeconds + " seconds", labelStyle);
        label.setPosition(uiStage.getWidth() / 2 - label.getWidth() / 2, uiStage.getHeight() - 150);
        uiStage.addActor(label);

        state = GameState.GAME_OVER;
    }

    @Override
    public void endContact(Contact contact) {

    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {

    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {

    }
}
