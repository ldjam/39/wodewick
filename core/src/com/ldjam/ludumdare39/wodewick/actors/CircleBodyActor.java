package com.ldjam.ludumdare39.wodewick.actors;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.ldjam.ludumdare39.wodewick.Game;

import java.util.List;
import java.util.stream.Collectors;

public abstract class CircleBodyActor extends Actor {

    static final float INITIAL_PHYSICS_RADIUS = 0.7f;
    Texture texture;
    final Body body;
    private final Fixture fixture;
    final List<Texture> textures;
    float physicsRadius = INITIAL_PHYSICS_RADIUS;

    protected abstract List<String> getTextureNames();

    CircleBodyActor(World world, float density) {
        textures = getTextureNames().stream().map(textureName -> new Texture(textureName)).collect(Collectors.toList());
        this.texture = this.textures.get(0);
        setSize(this.texture.getWidth() / Game.SCALING_FACTOR, this.texture.getHeight() / Game.SCALING_FACTOR);

        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;

        this.body = world.createBody(bodyDef);

        CircleShape circle = new CircleShape();
        circle.setRadius(this.physicsRadius * (getWidth() + getHeight()) / 2);

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = circle;
        fixtureDef.density = density;
        fixtureDef.friction = 0.4f;
        fixtureDef.restitution = 0.6f;

        this.fixture = body.createFixture(fixtureDef);
        this.fixture.setUserData(this);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(this.texture, getX() - getWidth() / 2, getY() - getHeight() / 2, getWidth(), getHeight());
    }

    @Override
    public void drawDebug(ShapeRenderer shapes) {
        shapes.setColor(0, 1, 0, 1);
        Vector2 position = this.body.getPosition();
        float radius = this.fixture.getShape().getRadius();
        shapes.circle(position.x, position.y, radius);
    }

    public void dispose() {
        this.fixture.setUserData(null);
        this.texture.dispose();
    }

    @Override
    protected void positionChanged() {
        this.body.setTransform(getX(), getY(), this.body.getAngle());
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        this.fixture.getShape().setRadius(this.physicsRadius * (getWidth() + getHeight()) / 2);

        Vector2 position = this.body.getPosition();
        setPosition(position.x, position.y);
    }
}
