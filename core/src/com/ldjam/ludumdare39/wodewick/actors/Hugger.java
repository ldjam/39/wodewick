package com.ldjam.ludumdare39.wodewick.actors;

import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Stage;

import java.util.*;

public class Hugger extends CircleBodyActor {
    private static final float VELOCITY = 15;

    private float nextVelocityUpdate;

    @Override
    protected List<String> getTextureNames() {
        return Arrays.asList(
                "hugger_blue.png",
                "hugger_pink.png"
        );
    }

    public Hugger(World world) {
        super(world, 0.5f);
        Random random = new Random();
        this.texture = this.textures.get(random.nextInt(this.textures.size()));
    }

    @Override
    protected void setStage(Stage stage) {
        super.setStage(stage);

        if (stage == null) {
            return;
        }

        Random random = new Random();
        float x = -getWidth() + random.nextInt(2) * (stage.getWidth() + 2 * getWidth());
        float y = -getHeight() + random.nextInt(2) * (stage.getHeight() + 2 * getHeight());
        setPosition(x, y);
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        nextVelocityUpdate -= delta;
        if (nextVelocityUpdate < 0) {
            Random random = new Random();

            Direction direction;
            if (getX() < getWidth()) {
                direction = Direction.RIGHT;
            } else if (getX() > getStage().getWidth() - 2 * getWidth()) {
                direction = Direction.LEFT;
            } else if (getY() < getHeight()) {
                direction = Direction.UP;
            } else if (getY() > getStage().getHeight() - 2 * getHeight()) {
                direction = Direction.DOWN;
            } else {
                float centerX = getStage().getWidth() / 2 - getWidth() / 2;
                float centerY = getStage().getHeight() / 2 - getHeight() / 2;

                List<Direction> directions = new ArrayList<>(Arrays.asList(Direction.values()));

                if (getX() < centerX) {
                    directions.add(Direction.RIGHT);
                } else {
                    directions.add(Direction.LEFT);
                }

                if (getY() < centerY) {
                    directions.add(Direction.UP);
                } else {
                    directions.add(Direction.DOWN);
                }

                direction = directions.get(random.nextInt(directions.size()));
            }

            this.body.setLinearVelocity(direction.x * VELOCITY, direction.y * VELOCITY);

            nextVelocityUpdate = random.nextFloat() * 2 - 1;
        }
    }
}
