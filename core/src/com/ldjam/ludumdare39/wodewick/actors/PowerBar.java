package com.ldjam.ludumdare39.wodewick.actors;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.ldjam.ludumdare39.wodewick.Game;

import java.util.Objects;

public class PowerBar extends Actor {

    private static final float BORDER_WIDTH = 5 / Game.SCALING_FACTOR;
    private final Wodewick wodewick;

    public PowerBar(Wodewick wodewick) {
        Objects.requireNonNull(wodewick);
        this.wodewick = wodewick;
        setSize(300 / Game.SCALING_FACTOR, 30 / Game.SCALING_FACTOR);
        setDebug(true);
    }

    @Override
    public void drawDebug(ShapeRenderer shapes) {
        shapes.set(ShapeRenderer.ShapeType.Filled);

        shapes.setColor(0.4f, 0.4f, 0.4f, 0.8f);
        shapes.rect(getX(), getY(), getWidth(), getHeight());

        float barWidth = (getWidth() - 2 * BORDER_WIDTH) * this.wodewick.currentPower / Wodewick.MAX_POWER;
        if (barWidth > 0) {
            float barAlpha = 0.8f;

            if (this.wodewick.currentPower < Wodewick.PUSH_POWER) {
                barAlpha /= 2;
            }

            shapes.setColor(0.0f, 0.0f, 0.0f, barAlpha);
            shapes.rect(getX() + BORDER_WIDTH, getY() + BORDER_WIDTH, barWidth, getHeight() - 2 * BORDER_WIDTH);
        }
    }

    @Override
    protected void setStage(Stage stage) {
        super.setStage(stage);

        if (stage == null) {
            return;
        }
        
        float padding = 10 / Game.SCALING_FACTOR;
        setPosition(stage.getWidth() - getWidth() - padding, stage.getHeight() - getHeight() - padding);
    }
}
