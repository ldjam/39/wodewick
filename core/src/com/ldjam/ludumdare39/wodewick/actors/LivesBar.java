package com.ldjam.ludumdare39.wodewick.actors;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.ldjam.ludumdare39.wodewick.Game;

import javax.xml.soap.Text;

public class LivesBar extends Actor {

    private final Wodewick wodewick;
    private final Texture texture;

    public LivesBar(Wodewick wodewick) {
        this.wodewick = wodewick;
        this.texture = new Texture("heart.png");
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        float x = getX();
        for (int i = 0; i < this.wodewick.numLives; i++) {
            batch.draw(this.texture, x, getY() - this.texture.getHeight() / Game.SCALING_FACTOR, this.texture.getWidth() / Game.SCALING_FACTOR, this.texture.getHeight() / Game.SCALING_FACTOR);
            x += 2 * this.texture.getWidth() / Game.SCALING_FACTOR;
        }

    }

    @Override
    public void drawDebug(ShapeRenderer shapes) {
        float radius = 20 / Game.SCALING_FACTOR;

        shapes.setColor(1, 0, 0, 0.7f);
        shapes.set(ShapeRenderer.ShapeType.Filled);

        float x = getX() + radius;
        for (int i = 0; i < this.wodewick.numLives; i++) {
            shapes.circle(x, getY() - radius, radius, 10);
            x += 3 * radius;
        }
    }

    @Override
    protected void setStage(Stage stage) {
        super.setStage(stage);

        if (stage == null) {
            return;
        }

        float padding = 5 / Game.SCALING_FACTOR;
        setPosition(padding, stage.getHeight() - padding);
    }
}
