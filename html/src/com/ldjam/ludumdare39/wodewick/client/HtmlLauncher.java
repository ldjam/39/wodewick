package com.ldjam.ludumdare39.wodewick.client;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.gwt.GwtApplication;
import com.badlogic.gdx.backends.gwt.GwtApplicationConfiguration;
import com.ldjam.ludumdare39.wodewick.Game;

public class HtmlLauncher extends GwtApplication {

    @Override
    public GwtApplicationConfiguration getConfig() {
        return new GwtApplicationConfiguration(Game.WIDTH, Game.HEIGHT);
    }

    @Override
    public ApplicationListener createApplicationListener() {
        return new Game();
    }
}